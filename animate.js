window.onload = () => {
    const VSHADER_SOURCE = `
    attribute vec4 a_Position;
    uniform mat4 u_ModelMatrix;
    void main(){
      gl_Position =u_ModelMatrix * a_Position;
    }`;

    const FSHADER_SOURCE = `
    precision mediump float;
    uniform vec4 u_FragColor;
    void main(){
      gl_FragColor = u_FragColor;
    }`;
    const canvas = document.getElementById('webgl');
    const gl = getWebGLContext(canvas);

    const ANGLE_STEP = 15.0;
    if (!gl) {
        console.log('Fail');
        return;
    }
    if (!initShaders(gl, VSHADER_SOURCE, FSHADER_SOURCE)) {
        console.log('Failed');
        return;
    }

    let a_Position = gl.getAttribLocation(gl.program, 'a_Position');
    let u_FragColor = gl.getUniformLocation(gl.program, 'u_FragColor');
    let u_ModelMatrix = gl.getUniformLocation(gl.program, 'u_ModelMatrix');

    if (a_Position < 0 || !u_FragColor) {
        return;
    }
    let positonAndColor = [];
    canvas.addEventListener('click', e => {
        let [x, y] = [e.clientX, e.clientY];
        let rect = e.target.getBoundingClientRect();
        x = (x - rect.left) / (rect.width / 2) - 1;
        y = 1 - (y - rect.top) / (rect.height / 2);
        positonAndColor = [
            [x, y, x, y - 0.1, x + 0.1, y, x + 0.1, y - 0.1],
            [Math.random(), Math.random(), Math.random(), Math.random()]
        ];
        let vertexBuffer = gl.createBuffer();
        if (!vertexBuffer) return;
        gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
        gl.clear(gl.COLOR_BUFFER_BIT);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positonAndColor[0]), gl.STATIC_DRAW);
        gl.vertexAttribPointer(a_Position, 2, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(a_Position);

        let radian = 2 * Math.PI * Math.random();
        let cosB = Math.cos(radian);
        let sinB = Math.sin(radian);

        let currentAngle = 0.0;
        let modelMatrix = new Matrix4();


        let g_last = Date.now();
        const animate = currentAngle => {
            let now = Date.now();
            let elapsed = now - g_last;
            g_last = now;
            let newAngle = currentAngle + (ANGLE_STEP * elapsed) / 1000.0;
            return newAngle % 360;
        };

        const tick = () => {
            currentAngle = animate(currentAngle);
            modelMatrix.setRotate(currentAngle, 0, 0, 1);
            gl.uniformMatrix4fv(u_ModelMatrix, false, modelMatrix.elements);
            gl.uniform4f(u_FragColor, ...positonAndColor[1]);
            gl.clear(gl.COLOR_BUFFER_BIT);
            gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
            requestAnimationFrame(tick);
        };

        tick();
    });
    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.clear(gl.COLOR_BUFFER_BIT);
};