window.onload = () => {
    const VSHADER_SOURCE = `
    attribute vec4 a_Position;
    void main(){
      gl_Position = a_Position;
      gl_PointSize = 4.0;
    }`;

    const FSHADER_SOURCE = `
    precision mediump float;
    uniform vec4 u_FragColor;
    void main(){
      gl_FragColor = u_FragColor;
    }`;
    const canvas = document.getElementById('webgl');
    const gl = getWebGLContext(canvas);
    if (!gl) {
        console.log('Fail');
        return;
    }
    if (!initShaders(gl, VSHADER_SOURCE, FSHADER_SOURCE)) {
        console.log('Failed');
        return;
    }

    let a_Position = gl.getAttribLocation(gl.program, 'a_Position');
    let u_FragColor = gl.getUniformLocation(gl.program, 'u_FragColor');

    if (a_Position < 0 || !u_FragColor) {
        return;
    }
    let positonAndColor = [];
    canvas.addEventListener('mousemove', e => {
        let [x, y] = [e.clientX, e.clientY];
        let rect = e.target.getBoundingClientRect();
        x = (x - rect.left) / (rect.width / 2) - 1;
        y = 1 - (y - rect.top) / (rect.height / 2);
        positonAndColor.push([
            [x, y],
            [Math.random(), Math.random(), Math.random(), Math.random()]
        ]);
        gl.clear(gl.COLOR_BUFFER_BIT);
        for (let val of positonAndColor) {
            gl.vertexAttrib3f(a_Position, val[0][0], val[0][1], 0.0);
            gl.uniform4f(u_FragColor, ...val[1]);
            gl.drawArrays(gl.POINTS, 0, 1);
        }
    });
    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.clear(gl.COLOR_BUFFER_BIT);
};