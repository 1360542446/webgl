window.onload = () => {
    const VSHADER_SOURCE = `
    attribute vec4 a_Position;
    uniform mat4 u_xformMatrix;
    void main(){
      gl_Position =u_xformMatrix * a_Position;
    }`;

    const FSHADER_SOURCE = `
    precision mediump float;
    uniform vec4 u_FragColor;
    void main(){
      gl_FragColor = u_FragColor;
    }`;
    const canvas = document.getElementById('webgl');
    const gl = getWebGLContext(canvas);

    const ANGLE = 0.0;
    if (!gl) {
        console.log('Fail');
        return;
    }
    if (!initShaders(gl, VSHADER_SOURCE, FSHADER_SOURCE)) {
        console.log('Failed');
        return;
    }

    let a_Position = gl.getAttribLocation(gl.program, 'a_Position');
    let u_FragColor = gl.getUniformLocation(gl.program, 'u_FragColor');
    let u_xformMatrix = gl.getUniformLocation(gl.program, 'u_xformMatrix');

    if (a_Position < 0 || !u_FragColor) {
        return;
    }
    let positonAndColor = [];
    canvas.addEventListener('click', e => {
        let [x, y] = [e.clientX, e.clientY];
        let rect = e.target.getBoundingClientRect();
        x = (x - rect.left) / (rect.width / 2) - 1;
        y = 1 - (y - rect.top) / (rect.height / 2);
        positonAndColor.push([
            [x, y, x, y - 0.1, x + 0.1, y, x + 0.1, y - 0.1],
            [Math.random(), Math.random(), Math.random(), Math.random()]
        ]);
        let vertexBuffer = gl.createBuffer();
        if (!vertexBuffer) return;
        gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
        gl.clear(gl.COLOR_BUFFER_BIT);
        for (let val of positonAndColor) {
            gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(val[0]), gl.STATIC_DRAW);
            gl.vertexAttribPointer(a_Position, 2, gl.FLOAT, false, 0, 0);
            gl.enableVertexAttribArray(a_Position);

            let radian = 2 * Math.PI * Math.random();
            let cosB = Math.cos(radian);
            let sinB = Math.sin(radian);
            // let xformMatrix = new Float32Array([
            //     cosB, sinB, 0.0, 0.0,
            //     -sinB, cosB, 0.0, 0.0,
            //     0.0, 0.0, 1.0, 0.0,
            //     0.0, 0.0, 0.0, 1.0,
            // ]);

            // let xformMatrix = new Float32Array([
            //     1.0, 0.0, 0.0, 0.0,
            //     0.0, 1.0, 0.0, 0.0,
            //     0.0, 0.0, 1.0, 0.0,
            //     -0.2, 0.2, 0.0, 1.0,
            // ]);

            let xformMatrix = new Float32Array([
                1.0, 0.0, 0.0, 0.0,
                0.0, 1.0, 0.0, 0.0,
                0.0, 0.0, 1.0, 0.0,
                0.0, 0.0, 0.0, 1.0,
            ]);
            gl.uniformMatrix4fv(u_xformMatrix, false, xformMatrix);
            gl.uniform4f(u_FragColor, ...val[1]);
            gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
        }
    });
    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.clear(gl.COLOR_BUFFER_BIT);
};