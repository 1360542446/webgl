window.onload = () => {
    const VSHADER_SOURCE = `
    attribute vec4 a_Position;
    void main(){
      gl_Position = a_Position;
      gl_PointSize=4.0;
    }`;

    const FSHADER_SOURCE = `
    void main(){
      gl_FragColor = vec4(0.0,1.0,0.0,1.0);
    }`;
    const canvas = document.getElementById('webgl');
    const gl = getWebGLContext(canvas);
    if (!gl) {
        console.log('Fail');
        return;
    }
    if (!initShaders(gl, VSHADER_SOURCE, FSHADER_SOURCE)) {
        console.log('Failed');
        return;
    }

    let a_Position = gl.getAttribLocation(gl.program, 'a_Position');

    const pointsTranslationPosition = (array, formerLineWidth) => {
        let lineWidth = formerLineWidth / 2;
        if (array.length < 1 || array.length % 2 !== 0) return;
        let newArray = [];
        for (let i = 0,
            length = array.length; i < length; i = i + 2) {
            if (i === 0) {
                let [X1, Y1, X2, Y2] = [array[i], array[i + 1], array[i + 2], array[i + 3]];
                let temp = Math.sqrt((Y1 - Y2) * (Y1 - Y2) + (X1 - X2) * (X1 - X2));
                let x1 = (Y1 - Y2) * lineWidth / temp;
                let y1 = (X2 - X1) * lineWidth / temp;
                newArray.push(X1 - x1);
                newArray.push(Y1 - y1);
                newArray.push(x1 + X1);
                newArray.push(y1 + Y1);
            } else if (i === length - 2) {
                let [X1, Y1, X2, Y2] = [array[i - 2], array[i - 1], array[i], array[i + 1]];
                let temp = Math.sqrt((Y1 - Y2) * (Y1 - Y2) + (X1 - X2) * (X1 - X2));
                let x1 = (Y1 - Y2) * lineWidth / temp;
                let y1 = (X2 - X1) * lineWidth / temp;
                newArray.push(X2 - x1);
                newArray.push(Y2 - y1);
                newArray.push(x1 + X2);
                newArray.push(y1 + Y2);
            } else {
                let [X1, Y1, X2, Y2] = [array[i - 2], array[i - 1], array[i + 2], array[i + 3]];
                let [Xmiddle, Ymiddle] = [array[i], array[i + 1]];

                let lineLength1 = Math.sqrt((Xmiddle - X1) * (Xmiddle - X1) + (Ymiddle - Y1) * (Ymiddle - Y1));
                let lineLength2 = Math.sqrt((X2 - Xmiddle) * (X2 - Xmiddle) + (Y2 - Ymiddle) * (Y2 - Ymiddle));// check linelength1/2 ===0

                let angle = Math.acos(((X1 - Xmiddle) * (X2 - Xmiddle) + (Y1 - Ymiddle) * (Y2 - Ymiddle)) /
                    (lineLength1 * lineLength2));
                let tempLineWidth = lineWidth / Math.sin(angle * 0.5);

                let [vectorX, vectorY] = [(Xmiddle - X1) / lineLength1 + (X2 - Xmiddle) / lineLength2,
                    (Ymiddle - Y1) / lineLength1 + (Y2 - Ymiddle) / lineLength2];
                let [pX, pY] = [0, 0];
                if (vectorX === 0 || vectorY === 0) {
                    if (vectorX === 0) {
                        if (vectorY > 0) {
                            pY = tempLineWidth;
                        } else if (vectorY < 0) {
                            pY = -tempLineWidth;
                        }
                    }

                    if (vectorY === 0) {
                        if (vectorX > 0) {
                            pX = tempLineWidth;
                        } else if (vectorY < 0) {
                            pX = -tempLineWidth;
                        }
                    }
                } else {
                    let temp = tempLineWidth / Math.sqrt(vectorX * vectorX + vectorY * vectorY);
                    pX = temp * vectorX;
                    pY = temp * vectorY;
                }
                // (-pY,pX)
                newArray.push(Xmiddle + pY);
                newArray.push(Ymiddle - pX);
                // newArray[newArray.length - 4];
                // newArray[newArray.length - 3];
                // console.log(newArray[newArray.length - 4], newArray[newArray.length - 3], newArray);
                // newArray.push(-pY + Xmiddle + newArray[newArray.length - 4]);
                // newArray.push(pX + Ymiddle + newArray[newArray.length - 4]);

                newArray.push(-pY + Xmiddle);
                newArray.push(pX + Ymiddle);
            }
        }
        return newArray;
    };

    let data = [
       
    ];
    /**
     * -0.2, 0.5,
        0.0, 0.0,
        0.3, -0.3,
        0.3, -0.5,
        0.5, -0.6,
        0.9, 0.8
     */
    for (let i = 0; i < 10000; i++) {
        if (Math.random() < 0.5) {
            data.push((-1) * Math.random());
        } else {
            data.push(Math.random());
        }
    }
    let now = new Date().getTime();
    
    let newData = pointsTranslationPosition(data, 2 / 200);
    let vertices = new Float32Array(newData);
    let vertexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(a_Position, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(a_Position);

    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.clear(gl.COLOR_BUFFER_BIT);
    gl.drawArrays(gl.TRIANGLE_STRIP, 0, data.length); // TRIANGLE_STRIP  LINE_STRIP POINTS
    console.log(new Date().getTime()-now);
};