window.onload = () => {
    const VSHADER_SOURCE = `
    attribute vec4 a_Position;
    void main(){
      gl_Position = a_Position;
      gl_PointSize=4.0;
    }`;

    const FSHADER_SOURCE = `
    precision mediump float;
    uniform vec4 u_FragColor;
    void main(){
      gl_FragColor = u_FragColor;
    }`;
    const canvas = document.getElementById('webgl');
    const gl = getWebGLContext(canvas);
    if (!gl) {
        console.log('Fail');
        return;
    }
    if (!initShaders(gl, VSHADER_SOURCE, FSHADER_SOURCE)) {
        console.log('Failed');
        return;
    }

    let a_Position = gl.getAttribLocation(gl.program, 'a_Position');
    let u_FragColor = gl.getUniformLocation(gl.program, 'u_FragColor');

    const pointsTranslationPosition = (array, formerLineWidth) => {
        let lineWidth = formerLineWidth / 2;
        if (array.length < 1 || array.length % 2 !== 0) return;
        let newArray = [];
        let formerFlag = false;
        for (let i = 0,
            length = array.length; i < length; i = i + 2) {
            if (i === 0) {
                let [X1, Y1, X2, Y2] = [array[i], array[i + 1], array[i + 2], array[i + 3]];
                let temp = Math.sqrt((Y1 - Y2) * (Y1 - Y2) + (X1 - X2) * (X1 - X2));
                let x1 = (Y1 - Y2) * lineWidth / temp;
                let y1 = (X2 - X1) * lineWidth / temp;
                newArray.push(X1);
                newArray.push(Y1);
                newArray.push(x1 + X1);
                newArray.push(y1 + Y1);
            } else if (i === length - 2) {
                let [X1, Y1, X2, Y2] = [array[i - 2], array[i - 1], array[i], array[i + 1]];
                let temp = Math.sqrt((Y1 - Y2) * (Y1 - Y2) + (X1 - X2) * (X1 - X2));
                let x1 = (Y1 - Y2) * lineWidth / temp;
                let y1 = (X2 - X1) * lineWidth / temp;
                if (!formerFlag) {
                    newArray.push(X2);
                    newArray.push(Y2);
                    newArray.push(x1 + X2);
                    newArray.push(y1 + Y2);
                } else {
                    newArray.push(x1 + X2);
                    newArray.push(y1 + Y2);
                    newArray.push(X2);
                    newArray.push(Y2);
                }
            } else {
                let [X1, Y1, X2, Y2] = [array[i - 2], array[i - 1], array[i + 2], array[i + 3]];
                let [Xmiddle, Ymiddle] = [array[i], array[i + 1]];

                let lineLength1 = Math.sqrt((Xmiddle - X1) * (Xmiddle - X1) + (Ymiddle - Y1) * (Ymiddle - Y1));
                let lineLength2 = Math.sqrt((X2 - Xmiddle) * (X2 - Xmiddle) + (Y2 - Ymiddle) * (Y2 - Ymiddle)); // check linelength1/2 ===0

                let angle = Math.acos(((X1 - Xmiddle) * (X2 - Xmiddle) + (Y1 - Ymiddle) * (Y2 - Ymiddle)) /
          (lineLength1 * lineLength2));
                let tempLineWidth = lineWidth / Math.sin(angle * 0.5);

                let [vectorX, vectorY] = [(Xmiddle - X1) / lineLength1 + (X2 - Xmiddle) / lineLength2,
                    (Ymiddle - Y1) / lineLength1 + (Y2 - Ymiddle) / lineLength2
                ];
                let [pX, pY] = [0, 0];
                if (vectorX === 0 || vectorY === 0) {
                    if (vectorX === 0) {
                        if (vectorY > 0) {
                            pY = tempLineWidth;
                        } else if (vectorY < 0) {
                            pY = -tempLineWidth;
                        }
                    }
                    if (vectorY === 0) {
                        if (vectorX > 0) {
                            pX = tempLineWidth;
                        } else if (vectorY < 0) {
                            pX = -tempLineWidth;
                        }
                    }
                } else {
                    let temp = tempLineWidth / Math.sqrt(vectorX * vectorX + vectorY * vectorY);
                    pX = temp * vectorX;
                    pY = temp * vectorY;
                }
                let p1X = -pY + Xmiddle;
                let p1Y = pX + Ymiddle;
                let flag = true;
                if (-vectorY * (X2 - Xmiddle) + vectorX * (Y2 - Ymiddle) < 0) { // obtuse angle
                    flag = false;
                }
                if (flag) { // 切自己
                    let [xl, yl] = [0, 0];
                    if (formerFlag === -1 || formerFlag === false) {
                        xl = -newArray[newArray.length - 2] + p1X + X1;
                        yl = -newArray[newArray.length - 1] + p1Y + Y1;
                    } else if (formerFlag === true) {
                        xl = -newArray[newArray.length - 4] + p1X + newArray[newArray.length - 2];
                        yl = -newArray[newArray.length - 3] + p1Y + newArray[newArray.length - 1];
                        newArray.push((p1X + newArray[newArray.length - 4]) / 2);
                        newArray.push((p1Y + newArray[newArray.length - 4]) / 2);

                    }
                    let b = 0;
                    let k = 0;
                    let [xr, yr] = [0, 0];
                    if (pX !== 0) {
                        k = (p1Y - Ymiddle) / (p1X - Xmiddle);
                        b = p1Y - k * p1X;
                        xr = (2 * k * yl + (1 - k * k) * xl - 2 * k * b) / (k * k + 1);
                        yr = yl + (xl - xr) / k;
                    }
                    newArray.push(xl);
                    newArray.push(yl);
                    newArray.push(-pY + Xmiddle);
                    newArray.push(pX + Ymiddle);
                    newArray.push(xr);
                    newArray.push(yr);
                } else {
                    console.log(formerFlag);
                    let [xl, yl] = [0, 0];
                    if (formerFlag === -1 || formerFlag === false) {
                        xl = newArray[newArray.length - 2] + Xmiddle - X1;
                        yl = newArray[newArray.length - 1] + Ymiddle - Y1;
                        if (formerFlag === false) {
                            newArray.push((Xmiddle + newArray[newArray.length - 4]) / 2);
                            newArray.push((Ymiddle + newArray[newArray.length - 4]) / 2);
                        }
                    } else if (formerFlag === true) {
                        xl = -newArray[newArray.length - 2] + Xmiddle + newArray[newArray.length - 4];
                        yl = -newArray[newArray.length - 1] + Ymiddle + newArray[newArray.length - 3];
                    }

                    let b = 0;
                    let k = 0;
                    let [xr, yr] = [0, 0];
                    if (pX !== 0) {
                        k = (p1Y - Ymiddle) / (p1X - Xmiddle);
                        b = p1Y - k * p1X;
                        xr = (2 * k * yl + (1 - k * k) * xl - 2 * k * b) / (k * k + 1);
                        yr = yl + (xl - xr) / k;
                    }

                    newArray.push(xl);
                    newArray.push(yl);
                    newArray.push(Xmiddle);
                    newArray.push(Ymiddle);
                    newArray.push(xr);
                    newArray.push(yr);
                }
                formerFlag = flag;
            }
        }
        return newArray;
    };

    let data = [
        -0.6027807030834245,
        0.09217682564203344,
        0.8728406500179149,
        0.4099527910748675,
        -0.7384590294108158,
        0.20560768370307536,
        -0.4302244604611858,
        0.6805696730500668,
        0.5839456999820534,
        0.45417400498427196];
    /**
   * 0.0, 0.5,
      0.0, 0.0,
      0.3, -0.3,
      0.3, -0.5,
      0.5, -0.6,
      0.9, 0.8
   */
    // for (let i = 0; i < 10; i++) {
    //     if (Math.random() < 0.5) {
    //         data.push((-1) * Math.random());
    //     } else {
    //         data.push(Math.random());
    //     }
    // }
    let newData = pointsTranslationPosition(data, 40 / 200);
    let vertices = new Float32Array(newData);
    let vertexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(a_Position, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(a_Position);

    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.clear(gl.COLOR_BUFFER_BIT);
    console.log(data, vertices);
    gl.uniform4f(u_FragColor, ...[0.0, 1.0, 0.0, 1.0]);
    gl.drawArrays(gl.TRIANGLE_STRIP, 0, vertices.length / 2); // TRIANGLE_STRIP  LINE_STRIP POINTS
    gl.uniform4f(u_FragColor, ...[1.0, 0.0, 0.0, 1.0]);
    gl.drawArrays(gl.POINTS, 0, vertices.length / 2 - 2);
    gl.uniform4f(u_FragColor, ...[1.0, 1.0, 0.0, 1.0]);
    gl.drawArrays(gl.LINE_STRIP, 0, vertices.length / 2);
};